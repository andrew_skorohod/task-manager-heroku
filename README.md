Приложение состоит из нескольких частей:
1.Task manager itself.
2.Tiny social network for task sharing and communication.
3.Ceasar cipher app.

Написано на Python. Фреймворк Django.
На фронтенде Bootstrap. Jquery для Ajax.

Инструкция для установки на локалке.

* git clone -b master "your link from the clone"
* virtualenv -p python3 task-manager
* cd task-manager
* source bin/activate
* pip install -r requirements.txt
* python manage.py migrate
* python manage.py runserver
* localhost:8000