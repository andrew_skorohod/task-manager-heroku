# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-09 11:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0003_msg'),
    ]

    operations = [
        migrations.AlterField(
            model_name='msg',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date'),
        ),
        migrations.AlterField(
            model_name='msg',
            name='title',
            field=models.CharField(max_length=200, verbose_name='topic'),
        ),
    ]
