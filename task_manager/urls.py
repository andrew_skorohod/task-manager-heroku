from django.conf.urls import url
from django.contrib import admin
from . import views


urlpatterns = [
    url(r'^$',(views.startpage),name="startpage"),
    # url(r'^create/$',(views.proj_create),name='proj_create'),
    url(r'^details/(?P<id>\d+)/task_detail/(?P<id_t>\d+)/$',(views.task_detail),name='task_detail'),
    # url(r'^details/(?P<id>\d+)/new_task/$',(views.task_create),name='task_create'),
    url(r'^details/(?P<id>\d+)/edit_task/(?P<id_t>\d+)/$',(views.task_update),name='task_edit'),
    url(r'^details/(?P<id>\d+)/del_task/(?P<id_t>\d+)/$',(views.task_delete),name='task_delete'),
    url(r'^details/(?P<id>\d+)/$',(views.proj_detail),name='details'),
    url(r'^delete/(?P<id>\d+)/$',(views.proj_delete),name='proj_delete'),
    url(r'^edit/(?P<id>\d+)/$',(views.proj_update),name='proj_edit'),
    url(r'^n_edit/$',(views.nProjectUpdate),name='n_proj_edit'),#for ajax
    url(r'^nt_edit/$',(views.nTaskUpdate),name='n_task_edit'),#for ajax
    url(r'^contact/$', (views.contact), name='contact'),
    url(r'^profile/(?P<id>\d+)/$', (views.profile), name='profile'),
    url(r'^userlist/$', (views.all_users), name='userslist'),
    url(r'^inbox/$', (views.inbox), name='inbox'),
    url(r'^outbox/$', (views.outbox), name='outbox'),
    url(r'^msg/(?P<id>\d+)/$',(views.message),name='message'),
    url(r'^msgdlt/(?P<id>\d+)/$',(views.msg_delete),name='msg_delete'),
    url(r'^msgsnd/(?P<id>\d+)/$', (views.send_message), name='snd'),
    url(r'^about/$', (views.about), name='about'),
]