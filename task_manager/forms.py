from django import forms
from .models import Project,Task,Msg
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.auth.models import User

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            'title',
        ]

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            'task_name',
            'task_priority',
            'task_status',
            'deadline',
        ]
        widgets = {'deadline':forms.DateInput(attrs={'type':'date'})}


class ContactForm(forms.Form):
    full_name = forms.CharField(label=_('Full name'))
    email = forms.EmailField()
    message = forms.CharField(widget = forms.Textarea,label=_('Message'))


class MyUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email','first_name', 'last_name')


class MessageForm(forms.ModelForm):
    class Meta:
        model = Msg
        fields = [
            'title',
            'content',
        ]
