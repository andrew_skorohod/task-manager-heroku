from django.contrib import admin
from .models import  Project,Task
# Register your models here.

class TaskInline(admin.TabularInline):
    model = Task
    fields = ['task_name','task_status','task_priority','deadline']
    extra = 1


class Project_Admin(admin.ModelAdmin):
    class Meta:
        model = Project

    inlines = [TaskInline]
    list_display = ['__str__','date','up_date']
    list_filter = ['date','up_date']
    search_fields = ['title',]

admin.site.register(Project, Project_Admin)