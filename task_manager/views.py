from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext as _

from .forms import ProjectForm, TaskForm, ContactForm, MyUserForm, MessageForm
from .models import Project, Task, Msg


# Create your views here.


def startpage(request):
    user = request.user
    if user.is_authenticated():
        project_list = Project.objects.filter(author=user.id).order_by('-date')
    else:
        return render(request, 'sign_pls.html')

    form = ProjectForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.author = request.user
        obj.save()
        return HttpResponseRedirect('/details/{0}/'.format(obj.id))

    context = {'title': 'list',
               'all_proj': project_list,
               'form': form,
               }
    return render(request, 'index.html', context)


def proj_detail(request, id):
    user = request.user
    if user.is_authenticated():
        proj = get_object_or_404(Project, id=id)
        tasks = Task.objects.filter(proj=proj.id).order_by('-date')
    form = TaskForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        print(obj.deadline)
        obj.proj = proj
        obj.save()
        return HttpResponseRedirect('/details/{0}/'.format(id))
    context = {
        'proj': proj,
        'tasks': tasks,
        'form': form,
    }
    return render(request, 'detail.html', context)


def proj_delete(request, id=None):
    if request.user.is_authenticated():
        obj = get_object_or_404(Project, id=id)
        obj.delete()
    return redirect("startpage")


def proj_update(request, id=None):
    if not request.user.is_authenticated():
        raise Http404
    instance = get_object_or_404(Project, id=id)
    form = ProjectForm(request.POST or None, instance=instance)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.save()
        return redirect("startpage")
    context = {'obj': instance,
               'form': form,
               }
    return render(request, 'create.html', context)


def task_detail(request, id, id_t):
    user = request.user
    if user.is_authenticated():
        # proj = get_object_or_404(Project,id= id)
        # tasks = Task.objects.filter(proj= proj.id).order_by('-date')
        task = get_object_or_404(Task, id=id_t)
    context = {
        # 'proj':proj,
        # 'tasks':tasks,
        'task': task,
    }
    return render(request, 't_detail.html', context)


def task_update(request, id, id_t):
    user = request.user
    if user.is_authenticated():

        task = get_object_or_404(Task, id=id_t)
        form = TaskForm(request.POST or None, instance=task)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            return HttpResponseRedirect('/details/{0}/'.format(id))
    context = {
        # 'proj':proj,
        # 'tasks':tasks,
        'task': task,
        'form': form,
    }
    return render(request, 't_create.html', context)


def task_delete(request, id, id_t):
    if request.user.is_authenticated():
        obj = get_object_or_404(Task, id=id_t)
        obj.delete()
    return HttpResponseRedirect('/details/{0}/'.format(id))


def nProjectUpdate(request):
    id = request.GET.get('id')
    instance = get_object_or_404(Project, id=id)
    formEdit = ProjectForm(request.POST or None, instance=instance)
    return HttpResponse(formEdit)


def nTaskUpdate(request):
    id = request.GET.get('id')
    instance = get_object_or_404(Task, id=id)
    formEdit = TaskForm(request.POST or None, instance=instance)
    return HttpResponse(formEdit)


def contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        form_email = form.cleaned_data.get("email")
        form_message = form.cleaned_data.get("message")
        form_full_name = form.cleaned_data.get("full_name")
        subject = _('Site contact form')
        from_email = settings.EMAIL_HOST_USER
        to_email = [from_email, ]
        contact_message = "user name '{0}'\n send us message : {1}.\n His email is  {2}".format(
                form_full_name,
                form_message,
                form_email)

        send_mail(subject,
                  contact_message,
                  from_email,
                  to_email,
                  fail_silently=True)
        messages.success(request, _('Thank you for feedback!!!'))

    context = {
        "form": form,
    }
    return render(request, "contact.html", context)


def about(request):
    return render(request, "about.html", )


def profile(request, id):
    if request.user.is_authenticated():
        if request.user.id == int(id):
            form = MyUserForm(request.POST or None, instance=request.user)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                return redirect("startpage")
            context = {
                'form': form,
            }
            return render(request, "my_page.html", context)
        else:
            instance = get_object_or_404(User, id=id)
            form = MessageForm(request.POST or None)
            context = {
                'instance': instance,
                'form': form,
            }
            return render(request, "profile.html", context)
    else:
        raise Http404('<h1>not authenticated</h1>')


def inbox(request):
    if request.user.is_authenticated():
        msgreceived = Msg.objects.filter(receiver=request.user)
        title = _('Inbox menu')
        context = {
            'messages': msgreceived,
            'title': title,
        }
        return render(request, "msg_list.html", context)


def outbox(request):
    if request.user.is_authenticated():
        msgsended = Msg.objects.filter(sender=request.user)
        title = _('Outbox menu')
        context = {
            'messages': msgsended,
            'title': title,
        }
        return render(request, "msg_list.html", context)


def send_message(request, id):
    form = MessageForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        sender = get_object_or_404(User, id=int(request.user.id))
        receiver = get_object_or_404(User, id=int(id))
        obj.sender = sender
        obj.receiver = receiver
        obj.save()
        return redirect("outbox")


def msg_delete(request, id=None):
    referer = request.META.get('HTTP_REFERER')
    if request.user.is_authenticated():
        obj = get_object_or_404(Msg, id=id)
        obj.delete()
    if 'inbox' in referer:
        return redirect("inbox")
    elif "outbox" in referer:
        return redirect("outbox")


def message(request, id):
    if request.user.is_authenticated():
        msg = get_object_or_404(Msg, id=id)
        form = MessageForm(request.POST or None)
        context = {
            'msg': msg,
            'form': form,
        }
        return render(request, "message.html", context)
    else:
        raise Http404('<h1>not authenticated</h1>')


def all_users(request):
    if request.user.is_authenticated():
        userlist = User.objects.all().order_by('id')
        query = request.GET.get('query')
        if query:
            userlist = userlist.filter(Q(username__icontains=query))
        form = MessageForm(request.POST or None)
        context = {
            'userlist': userlist,
            'form': form,
        }
        return render(request, "userslist.html", context)
    else:
        raise Http404('<h1>not authenticated</h1>')
