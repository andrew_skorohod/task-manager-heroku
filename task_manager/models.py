from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class Project(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, verbose_name=_("author"))
    title = models.CharField(max_length=200, verbose_name=_("title"))
    date = models.DateTimeField('date published', auto_now=False, auto_now_add=True, )
    up_date = models.DateTimeField('last update', auto_now=True, auto_now_add=False, )

    def __str__(self):
        return self.title


# models.BooleanField(default=False)
class Task(models.Model):
    TASK_PRIORITY = (("A", "status A"),
                     ("B", "status B"),
                     ("C", "status C"),
                     ("D", "status D"),
                     ("E", "status E"),
                     )
    TASK_STATUS = (
        ('Done', 'done'),
        ('Current', 'current'),
    )
    proj = models.ForeignKey(Project, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=200, verbose_name=_("Task name"))
    task_priority = models.CharField(max_length=7, choices=TASK_PRIORITY, verbose_name=_("Task priority"))
    task_status = models.CharField(max_length=7, choices=TASK_STATUS, default='Current', verbose_name=_("Task status"))
    date = models.DateTimeField('date published', auto_now=False, auto_now_add=True)
    up_date = models.DateTimeField('last update', auto_now=True, auto_now_add=False)
    deadline = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.task_name


class Msg(models.Model):
    title = models.CharField(max_length=200, verbose_name=_("topic"))
    content = models.TextField()
    sender = models.ForeignKey(User, default=1, verbose_name=_("author"), related_name='author')
    receiver = models.ForeignKey(User, default=1, verbose_name=_("receiver"), related_name='receiver')
    date = models.DateTimeField('date', auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.title

#
#
