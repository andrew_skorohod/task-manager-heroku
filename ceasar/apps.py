from django.apps import AppConfig


class CeasarConfig(AppConfig):
    name = 'ceasar'
