# -*- coding: utf-8 -*-
import math
from django.shortcuts import render
from . import forms


# guessing alghorhytm was taken from here https://www.nayuki.io/res/automatic-caesar-cipher-breaker-javascript.js

def ceasar_main(request):
    form = forms.CeasarForm((request.POST or None))
    if form.is_valid():
        text = form.cleaned_data.get("content")
        key = int(form.cleaned_data.get("key"))
        status = form.cleaned_data.get("status")
        letter_count = diagram(text)

        if status == "decr":
            temp = getAllEntropies(text)
            guess = getBestEntropy(temp)
            new_text = decrypt(text, key)
            context = {
                'form': form,
                'new_text': new_text,
                'text': text,
                'guess': guess,
                'letter_count': letter_count,
            }
            return render(request, 'ceasar.html', context)

        else:
            new_text = encrypt(text, key)
            context = {
                'form': form,
                'new_text': new_text,
                'text': text,
                'letter_count': letter_count,
            }
            return render(request, 'ceasar.html', context)
    context = {
        'form': form,
    }
    return render(request, 'ceasar.html', context)


ENGLISH_FREQS = [
    0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, 0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406,
    0.06749, 0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758, 0.00978, 0.02360, 0.00150, 0.01974, 0.00074]


def getEntropy(string):
    sum = 0
    ignored = 0
    for i in string:
        c = ord(i)
        if c >= 65 and c <= 90:
            sum += math.log(ENGLISH_FREQS[c - 65])
        elif c >= 97 and c <= 122:
            sum += math.log(ENGLISH_FREQS[c - 97])
        else:
            ignored += 1
    return -sum / math.log(2) / (len(string) - ignored)


def getAllEntropies(string):
    result = []
    for i in range(26):
        result.append([i, getEntropy(decrypt(string, i))])
    return result


def decrypt(text, key):
    key = -key
    new_text = ''
    for letter in text:
        if letter.isalpha():
            num = ord(letter)
            num += key
            if letter.isupper():
                if num > ord('Z'):
                    num -= 26
                elif num < ord('A'):
                    num += 26
            elif letter.islower():
                if num > ord('z'):
                    num -= 26
                elif num < ord('a'):
                    num += 26
            new_text += chr(num)
        else:
            new_text += letter
    return new_text


def encrypt(text, key):
    new_text = ''
    for letter in text:
        if letter.isalpha():
            num = ord(letter)
            num += key
            if letter.isupper():
                if num > ord('Z'):
                    num -= 26
                elif num < ord('A'):
                    num += 26
            elif letter.islower():
                if num > ord('z'):
                    num -= 26
                elif num < ord('a'):
                    num += 26
            new_text += chr(num)
        else:
            new_text += letter
    return new_text


def getBestEntropy(all):
    temp = 100
    for i in all:
        if float(i[1]) < temp:
            temp = float(i[1])
            result = i[0]
    return result


def diagram(string):
    letter_count = dict(a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0, i=0, j=0, k=0, l=0, m=0, n=0, o=0, p=0, q=0, r=0, s=0,
                        t=0, u=0, v=0, w=0, x=0, y=0, z=0)
    emtpty = []
    result = []
    try:
        for letter in string:
            if letter.isalpha():
                # t_list.append(letter.lower())
                letter = letter.lower()
                letter_count[letter] += 1

        for key, value in letter_count.items():
            if value > 0:
                result.append([key, value])
        return result
    except:
        return emtpty
