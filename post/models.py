from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
# Create your models here.
class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,default = 1)
    title = models.CharField(max_length=120)
    image = models.FileField(null=True,blank=True)
    content = models.TextField()
    date = models.DateTimeField('date published',auto_now=False,auto_now_add=True)
    up_date = models.DateTimeField('last update',auto_now=True,auto_now_add=False,)
    def __str__(self):
        return self.title

    # def absolute_url(self): #allows to use abs url  to get page with it's id but it's to complicated...
    #     return reverse('details',kwargs={'id':self.id})