from django.contrib import admin
from .models import Post
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    class Meta():
        model = Post

    list_display = ['__str__','date','up_date']
    list_filter = ['date','up_date']
    search_fields = ['title','content']



admin.site.register(Post,PostAdmin)