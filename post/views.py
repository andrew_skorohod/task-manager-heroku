from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect

from .forms import PostForm
from .models import Post


# Create your views here.

def post_list(request):
    queryset_list = Post.objects.all().order_by('-date')  # this thing does sort by date
    # search form starts
    query = request.GET.get('query')  # query  is in quotes is in list template form
    if query:
        queryset_list = queryset_list.filter(Q(title__icontains=query) |
                                             Q(content__icontains=query)).distinct()

    # sometimes gives intendation error cuz  u should be careful with that | and spaces
    # Q allows to search everywhere in post ,notice that sign '|' between querys !!! #distinct blocks dublicates.
    # search form ends
    # paginator starts
    paginator = Paginator(queryset_list, 5)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    # paginator ends
    context = {'title': 'list',
               'all_posts': queryset,
               }
    return render(request, 'list_p.html', context)


def post_create(request):
    if not request.user.is_superuser:  # if its a regular user raises 404
        raise Http404
    form = PostForm(request.POST or None, request.FILES or None)  # hide this field is required
    if form.is_valid():  # request.FILES or None allows to add files. (magic o_O)
        obj = form.save(commit=False)
        obj.save()
        return HttpResponseRedirect(
            '/devblog/details/{0}/'.format(obj.id))  # redirects to details when created(abs path)
    context = {'form': form,
               'title': 'Create new post'
               }
    return render(request, 'create_p.html', context)


def post_detail(request, id):
    obj = get_object_or_404(Post, id=id)
    context = {'title': obj.title,
               'obj': obj,
               }
    return render(request, 'detail_p.html', context)


def post_delete(request, id=None):
    if not request.user.is_staff or request.user.is_superuser:
        raise Http404
    obj = get_object_or_404(Post, id=id)
    obj.delete()
    # messages.success(request,'Post was deleted')       replace it   with JS  message!!!!
    return redirect('home')


def post_update(request, id=None, ):
    if not request.user.is_staff or request.user.is_superuser:
        raise Http404
    obj = get_object_or_404(Post, id=id)
    form = PostForm(request.POST or None, request.FILES or None,
                    instance=obj)  # hide this field is required and check if post then dp smth
    if form.is_valid():  # instance adds instance of obj.title and obj.content to form as it already typed in
        obj = form.save(commit=False)
        obj.user = request.user  # dont know should i make it that way cuz it's refreshes author !!!
        obj.save()
        return HttpResponseRedirect(
            '/devblog/details/{0}/'.format(obj.id))  # redirects to details when updated(abs path)
    context = {'title': obj.title,
               'obj': obj,
               'form': form,
               }
    return render(request, 'create_p.html', context)
